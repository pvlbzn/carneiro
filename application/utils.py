import os

def get_envar(name):
    if name in os.environ:
        return os.environ[name]
    else:
        raise Exception('missing environment variable')


def find_item(obj, key):
    item = None
    if key in obj:
        return obj[key]
    for k, v in obj.items():
        if isinstance(v, dict):
            item = find_item(v, key)
            if item is not None:
                return item


def keys_exist(obj, keys):
    for key in keys:
        if find_item(obj, key) is None:
            return (False)
    return (True)

# Name of the table in DynamoDB on AWS
TABLE_NAME = 'hunglishTable'

# System messages
COMMANDS = [
    '7dfb4cf67742cb0660305e56ef816c53fcec892cae7f6ee39b75f34e659d672c', # users
]
