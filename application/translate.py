import random
import xml.etree.ElementTree as ET

from utils import COMMANDS, get_envar
from botocore.vendored import requests

class Responses:
    def __init__(self):
        self.en_pt = [
            'If you can’t spell in 🇺🇸 how do you expect me to translate for you! 😭 #robotproblems',
            '🙄 Wow that word.... is a little um... hard to understand. Can you try that again?',
            "You've confused me... try spelling that again, but this time correctly 😂 ",
            "Some think I'm a genius 🤔 but I'm not I swear I'm just a robot, so could you please, please try spelling that again for me? ",
            "👨🏻‍💻💩... What?",
            "1010 1010 1010 1011. See you don't understand my language so how am I meant to understand your spelling today?",
            "I'm sorry I slept terrible last night, just kidding I can't sleep i'm a robot. Anyways, I don't understand what you just wrote can you re-type it for me?",
            "This is the spelling police 👮🏻 and you're under arrest. Please try again.",
            "I think you spelt that wrong. My advice always start the day with a good breakfast 🍳 🥓 🥞",
        ]

        self.pt_en = [
            "Se você não consegue escrever em inglês como você espera que eu traduza para você? 😭 #robotproblems",
            "Puxa! Essa palavra... é um pouco... difícil de entender. Você pode tentar novamente?",
            "🙄 Você me deixou confuso... tente escrever novamente, mas dessa vez corretamente  😂 ",
            "Algumas pessoas acham que eu sou um gênio 🤔 mas eu não sou, eu juro, sou apenas um robô. Então você poderia, por favor, tentar escrever isso novamente para mim?",
            "👨🏻‍💻💩... O que?",
            "1010 1010 1010 1011. Viu só? Você não entende minha linguagem, então como eu deveria entender o que você está escrevendo hoje?",
            "Desculpe, eu dormi muito mal noite passada. Brincadeira, eu não durmo, eu sou um robô! 🤖 De qualquer forma, eu não entendi o que você acabou de escrever, poderia digitar isso novamente para mim?",
            "Aqui é a polícia da digitação 👮🏻 e você está detido. Por gentileza, tente novamente. ",
            "Eu acho que você digitou isso errado. Um conselho: sempre comece o dia com um café da manhã reforçado!🍳 🥓 🥞",
            " Yo no comprendo. 😖🇪🇸 ¿es español?"
            ]
            
    
    def en_to_pt(self):
        return random.choice(self.en_pt)
    
    def pt_to_en(self):
        return random.choice(self.pt_en)

class Translator:
    turl = 'https://api.microsofttranslator.com/V2/Http.svc/Translate'
    curl = 'https://api.cognitive.microsoft.com/sts/v1.0/issueToken'
    key = get_envar("MS_TRANSLATE")
    req_header = {'Ocp-Apim-Subscription-Key': key}

    def __init__(self, from_lang, to_lang):
        self.from_lang = from_lang
        self.to_lang = to_lang
        self.responser = Responses()
    
    def translate(self, word):
        res = requests.post(Translator.curl, headers=Translator.req_header)
        token = res.text

        text = word

        source_lang = self.from_lang
        target_lang = self.to_lang

        params = {
            'appid': 'Bearer ' + token,
            'text': text,
            'from': source_lang,
            'to': target_lang
        }

        req_header = {'Accept': 'application/xml'}
        res = requests.get(Translator.turl, params=params, headers=req_header)

        xml = ET.fromstring(res.text)
        txt = xml.text

        return txt
    
    def verify(self, translated, original):
        if translated == original and translated not in COMMANDS:
            if self.from_lang == 'en' and self.to_lang == 'pt':
                return self.responser.en_to_pt()
            elif self.from_lang == 'pt' and self.to_lang == 'en':
                return self.responser.pt_to_en()
        else:
            return translated
