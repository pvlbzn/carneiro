## Result

Here is the result so far. Consider it is a beta version.

I've created a mockup page which contains our bot, you can try to use it here:

```
https://www.facebook.com/Hunglish-167263503897973/
```

![img](https://gitlab.com/pvlbzn/carneiro/raw/master/img/chat.png)

Current functionality:

* Translate from English to Português
* Translate from Português to English
* Toggle Languages

This is done by using a database on the backend side. Bot remembers its users
and their preferences.

**Issues:** There is only one tech issue: free trier translation API works really
slow. It takes about 2 seconds for a translation. However, it won't get worse
when there will be lots of clients. It is only per-request-issue.

**Solution:** I used Facebook feature which shows that our bot actually types at
the moment and not dead or hang. Try to talk to it, I guess you will find it as
a pleasant experience.



## How to Setup Greeting Message

We aren't allowed to send a message from the bot itself, however, you can
setup a greeting message by hands on your facebook page:

```
https://www.facebook.com/YOUR_NAME_HERE/settings/?tab=messaging
```

Substitute `YOUR_NAME_HERE` with your page's name and select "Messaging" 
in the left menu. Scroll down and you will find "Show a Messenger Greeting".
That's what you need to activate and change.


## Translation Experience

### Failure Handling

For whatever reason Microsoft API doesn't have an error handling in a classical
meaning. Here are some examples, input on the left-hand side, an output on the right-hand side:

```
# 1. Correct input
translate('How are you?')                -> Como está?

# 2. Misstyped input
translate('What re you doin')            -> O que está fazendo

# 3. Badly misstyped word
translate('aintsshasd!!')               -> aintsshasd!!!!

# 4. Badly misstyped sentence
translate('aintsshasd aint daskhdgg!!') -> dsfkjh não é daskhdgg!!!!
```

###### Correct Input

No problems here, everything is handled nice.


###### Misstyped Input

Since modern machine translation is based on machine learning algorithm which
is capable of *recovering* mistyped words/sentences our mistyped input was
corrected and translated.


###### Badly Misstyped Word

Instead of an error, Microsoft returns a word which wasn't translated. However,
because this word is the same in relation to the original input we can handle it
easily by the following logic:

```
if translated text is equal to input text
    return a funny sentence
```


###### Badly Misstyped Sentence

Here we have a problem. Because text is partially translated and partially
mirrored. So algorithm tried to translate/recover mistyped pieces. As we can
see original input is somewhat similar to translated output, but not the same.

Let's mark this case as **TODO**. I know how to handle it, but lets first build
a working product and focus on details later on.



## Hybrid System

**Task:** process some user messages by human

**Challenge:** There is no straightforward or built-in way of doing that. Hybrid
bots aren't supported yet. However, we always can solve it by some custom solution.

### Solution

Study Case: user chats with the bot for a while and once he/she is up to talking
to some account manager or another human user can send a message:

> contact a human

or 

> speak to human

And from this point, we can read this message automatically and send it to someone.
I see many possible solutions for this scenario, but these 3 are probably best:


##### Send Email

From our code, we can send an email to some preset email address, like manager@yourbusiness.com,
with client's details. A manager can go to the bot page on facebook, find the
chat with the user and start to talk to him/her.


##### Send SMS

We can setup service such as Twilio to send an SMS to a certain event. The same
logic as with an email, but instead of email we will use SMS.


##### Send Message in Facebook

This one probably ugliest and easiest but not really reliable. We can setup
some account by an encrypted message (like that one which I sent to you which 
returns # of users) and this account will start to receive the same messages
as email or SMS version, but inside the facebook.



## Broadcasting System

Facebook has broadcasting service **but** we can't use it for promotions without
violations of the terms and conditions as stated [here](https://developers.facebook.com/docs/messenger-platform/send-messages/broadcast-messages#requirements)

> Messages sent using the Broadcast API must be non-promotional and adhere to the Messenger Platform's [subscription messaging policy](https://developers.facebook.com/docs/messenger-platform/policy/policy-overview#subscription_messaging).

But is exactly what we want to, eventually, do.

I guess that it is better to not violate the terms and conditions bc our service
simply can be banned and we will lose all the client base since *client ID is
page-bound or local*. That means that you have 1 facebook account, but each
chat bot knows you by different IDs. Therefore if the bot will be banned our collected
The ID cannot be reused by any means.


### Solution

We can use existing database infrastructure. Here how it works:

When a new user sends the first message to the bot it automatically recorded into
the database. Later using encrypted messages system (like that encrypted command
which replies back with # of users) we can spread a message to all known IDs,
however this is not in *official* docs, therefore we have to try it first to understand
whether it works or not.

