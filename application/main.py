# Facebook Bot
#
# There are 2 kinds of possible messages:
#   * User message
#   * System message
#
# User message is the most frequent message by far. System message
# used only once for setting up the bot.
#
# Each umessage has a pipeline:
# dispatch -> process -> send result back
#
# Dispatching step tries to understand what kind of a message it was
# and what to send back.
#
# Process translates message and after message is processed it sent
# back.
#
# Pavel Bazin | pavelbazin.com | 2018
#

import os
import json
import time
import boto3

from enum import Enum
from utils import get_envar, find_item, keys_exist, TABLE_NAME, COMMANDS
from translate import Translator
from botocore.vendored import requests


class Status(Enum):
    MARK = 'mark_seen'
    TYPING = 'typing_on'
    NTYPING = 'typing_off'


class MessageKind(Enum):
    BUTTON_MESSAGE = 0
    TEXT_MESSAGE = 1
    GETTING_STARTED_MESSAGE = 2
    UNKNOWN = 3


class Message:
    def __init__(self, uid):
        self.params = {"access_token": os.environ['access_token']}
        self.headers = {"Content-Type": "application/json"}
        self.uid = uid

    def _send(self, data):
        r = requests.post(
            "https://graph.facebook.com/v2.6/me/messages",
            headers=self.headers,
            params=self.params,
            data=data)

        if r.status_code != 200:
            print(r.status_code)
            print(data)
            print(r.text)


class TextMessage(Message):
    def __init__(self, uid, message):
        super().__init__(uid)
        self.message = message

    def send(self):
        if self.message:
            data = json.dumps({
                "recipient": {
                    "id": self.uid
                },
                "message": {
                    "text": self.message
                }
            })
        else:
            data = ''
        return super()._send(data)


class StatusMessage(Message):
    def __init__(self, uid):
        super().__init__(uid)

    def send(self, status):
        if status:
            data = json.dumps({
                "recipient": {
                    "id": self.uid
                },
                "sender_action": status.value
            })
        else:
            data = ''
        return super()._send(data)


# Defaults to template button message
class TemplateMessage(Message):
    def __init__(self, uid):
        super().__init__(uid)

    def send(self, en=True):
        data = json.dumps({
            "recipient": {
                "id": self.uid
            },
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type":
                            "button",
                        "text":
                            "Ok let's go 🤓! What language are we translating today?" if en else "Ok, vamos lá! 🤓 Em que idioma estamos traduzindo hoje?",
                        "buttons": [
                            {
                                "type": "postback",
                                "title": "🇧🇷 to 🇺🇸" if en else "🇧🇷 para 🇺🇸",
                                "payload": "pt_to_en"
                            },
                            {
                                "type": "postback",
                                "title": "🇺🇸 to 🇧🇷" if en else "🇺🇸 para 🇧🇷",
                                "payload": "en_to_pt"
                            },
                        ]
                    }
                }
            }
        })
        return super()._send(data)


# Greeting message is a little different from other message classes
# because it already uses them, not extends
class GreetingMessage:
    def __init__(self, uid):
        self.uid = uid

    def send(self):
        '''Send starts a greeting routine which is made out of
        a sequence of messages.'''
        messages = [
            "Heyyy guys! 🤖 Rhavi-bot here! Are you ready to test me out?🤘",
            "Ok... I'm now now your new personal pocket translator 😇",
            "I'm here to help you find that missing word or sentence 😡 for when you're practising your English or travelling overseas with robot speed... Much quicker than the real Rhavi 🤣",
            "... and some say more handsome too 😝😳",
            "Remember you always can change the selected language later, just type 'change language'  🤓 and I'll send you the options!"
        ]

        status = StatusMessage(self.uid)

        status.send(Status.TYPING)
        # for msg in messages:
        for message in messages:
            self.send_submessage(message, status)

        status.send(Status.NTYPING)

        status.send(Status.TYPING)
        status.send(Status.NTYPING)
        TemplateMessage(self.uid).send()

    def send_submessage(self, message, status):
        status.send(Status.TYPING)
        # Translator.translate()
        status.send(Status.NTYPING)
        TextMessage(self.uid, message).send()


class Model:
    def __init__(self):
        self.db = boto3.client('dynamodb')

    def add(self, uid, pref):
        self.db.put_item(
            TableName=TABLE_NAME,
            Item={'ClientID': {
                'S': str(uid)
            },
                'Preference': {
                    'S': pref
                }})

    def get(self, uid):
        item = self.db.get_item(
            TableName=TABLE_NAME, Key={'ClientID': {
                'S': str(uid)
            }})

        if 'Item' in item.keys():
            return item['Item']['Preference']['S']
        else:
            return None


def accept_challenge(event):
    verification_token = str(find_item(event, 'hub.verify_token'))
    challenge = int(find_item(event, 'hub.challenge'))
    if os.environ['verify_token'] == verification_token:
        return challenge


# problem that button press doesn't parser correctly
def dispatcher(event):
    if keys_exist(event, ['body-json', 'entry']):
        event_entry = event['body-json']['entry'][0]

        if keys_exist(event_entry, ['standby']):
            return MessageKind.GETTING_STARTED_MESSAGE

        if keys_exist(event_entry, ['messaging']):
            messaging_event = event_entry['messaging'][0]

            # Button event
            if keys_exist(messaging_event, ['postback']):
                return MessageKind.BUTTON_MESSAGE
            # Regular message event
            if keys_exist(messaging_event, ['message']):
                return MessageKind.TEXT_MESSAGE
            else:
                return MessageKind.UNKNOWN


def process_text_message(event, model):
    # Get user data from request
    entry = event['body-json']['entry'][0]
    messaging_event = entry['messaging'][0]
    sender_id = messaging_event['sender']['id']
    text = messaging_event['message']['text']

    pref = model.get(sender_id)
    status = StatusMessage(sender_id)

    status.send(Status.TYPING)

    # TODO: refactor
    if pref is not None:
        if pref == 'pt_to_en':
            en = False
        else:
            en = True
    else:
        en = False

    if text.lower() == 'change language' or text.lower() == 'mudar idioma':
        status.send(Status.NTYPING)
        return TemplateMessage(sender_id).send(en)

    # If user is known to the system
    if pref is not None:
        print('pref isnt none')
        if pref == 'pt_to_en':
            t = Translator('pt', 'en')
            msg = t.verify(t.translate(text), text)
        elif pref == 'en_to_pt':
            t = Translator('en', 'pt')
            msg = t.verify(t.translate(text), text)
        else:
            raise Exception('unknown preference: {0}'.format(pref))

        status.send(Status.NTYPING)
        TextMessage(sender_id, msg).send()

    # If user is new to the system
    else:
        # Send a sequence of messages
        GreetingMessage(sender_id).send()


def process_button_message(event, model):
    entry = event['body-json']['entry'][0]
    messaging_event = entry['messaging'][0]
    sender_id = messaging_event['sender']['id']

    data = messaging_event['postback']['payload']
    model.add(sender_id, data)

    pref = model.get(sender_id)
    if pref is None:
        en = True
    else:
        if pref == 'pt_to_en':
            en = False
        else:
            en = True

    msg = TextMessage(sender_id, "Alright we're good to go! Remember you always can change the selected language later, just type 'change language'  🤓 and I'll send you the options! Test me out by writing your word or sentence below 😜" if en else "Muito bem, estamos prontos para começar! Lembre-se de que você sempre pode mudar o idioma selecionado mais tarde, é só digitar 'mudar idioma' 🤓 e você verá as opções. Faça um teste escrevendo uma palavra ou uma frase abaixo. 😜")
    msg.send()


def process_getting_started(event, model):
    entry = event['body-json']['entry'][0]
    sby = entry['standby'][0]
    uid = sby['sender']['id']
    message = sby['postback']['title']

    if message == 'Get Started':
        GreetingMessage(uid).send()
    else:
        raise Exception('unknown message from postback')


def lambda_handler(event, context):
    model = Model()

    # Challenge must be handled on this level because it simply must
    # return a string.
    if keys_exist(event, ["params", "querystring", "hub.verify_token", "hub.challenge"]):
        return accept_challenge(event)

    kind = dispatcher(event)

    if kind == MessageKind.TEXT_MESSAGE:
        print('message kind: TEXT_MESSAGE')
        process_text_message(event, model)
        return
    elif kind == MessageKind.BUTTON_MESSAGE:
        print('message kind: BUTTON_MESSAGE')
        process_button_message(event, model)
        return
    elif kind == MessageKind.GETTING_STARTED_MESSAGE:
        print('message kind: GETTING_STARTED_MESSAGE')
        process_getting_started(event, model)
        return
    elif kind == MessageKind.UNKNOWN:
        raise Exception('unknown kind of a message')

    return (None)
