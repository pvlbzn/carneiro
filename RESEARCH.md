# Abstract

## Objective

Produce a reliable translation service based on Messenger platform.


#### Project Parts

The project has 3 logical parts:

* Facebook
* Translation
* Backend


#### Project Features

MVP level features:

* Translate received word from Português -> English


Candidate features:

* Spanish -> English Option
* Toggle between English -> Português and vice versa
* Marketing Broadcast
* Failure Handling
* Scalability


Proposals:

* Translate Sentences
* Expand to More Platforms



Comments:

Additional languages aren't a problem, however, they require time for implementation.
Scalability won't be an issue. In fact, our system will be capable to handle ~1,000
messages per second.

<br>
<br>
<br>

# Product Research

Let's take a look at other bots and what is the difference between *good and bad*.


## Existing Bots

#### Instant Translator

[Instant Translator](https://chatbotsmagazine.com/how-instant-translator-bot-got-1-million-users-27089533df32)
bot got its attention due to the good user handling and good availability metrics.
Also, they are available on two platforms: Viber and Facebook.

 - reviews
 - failure processing with a failed word
 - marketing message on “get started”


#### Other Bots

We can find a great list [here](https://botlist.co/platforms/messenger/most-liked).
All these bots have a common trait -- they do care about their users and they do
care about [Bot User Experience](https://medium.com/ux-for-bots/ux-for-bots-must-read-articles-123e744a7ff1).

While all these bots are quite different they share one thing: all of them are
quite narrow, or better to say each of these bots has its own specialization.

We also can find many unsuccessful translation bots because they just trying
to target everything at once, have a poor user experience and weak tech stack
behind them.


#### Conclusion

We must:

* Provide Reliable Service
* Care About User Experience
* Target Particular Market Group


## User Experience

We should handle our clients gently. Here are the two things which we can do:

* Use Greeting Messages
* Use Facebook UI Features


**Greeting Message:** This thing is up to you or your marketing team. Think what
should you say to greet your users? Since our bot is somewhat a "special commercial product"
it will have some marketing stuff in it. But it is better to avoid "right in the face"
approach.

**[Facebook UI Utilities](https://developers.facebook.com/docs/messenger-platform/send-messages/templates):** We should think how to use them best.

**Failure Handling:** What should we answer on "(*U@euhi)fkg??" user query?


### Limitations

Important note. Since we are using 3-rd party services everywhere (Facebook, translation)
some User Experience features will strongly depend on these services facilities.
For example Translation Service might have a field like a "suggestion" in case
if a word wasn't recognized, but also it may not have it.

Therefore, **some ideas and improvements will arise during the development phase**.

---

<br>
<br>
<br>

# Development Choices

## Translation Services

**Problem Statement:**
We are looking for some 3-rd party API (functionality) which is highly reliable,
has a minimal downtime and has a good latency (feedback speed).

**Word vs Sentence Difference:**
While translating a word, one word, is not a problem since it has no specified
context, translating a sentence is difficult and not all services got it right.

**Limitations:**
Each Cloud service has its own limitations, prices, performance. Also, we must
understand that even if our part of the development is perfect we cannot handle
3-rd party failures. Therefore, we should find the most reliable one.

### Research

I found the following research on Machine Translation Quality:

![research](http://mt-quality.multilizer.com/wp-content/uploads/2014/07/pt-machine-translation-quality-scores-2015-04.jpg)

While there are [a lot](https://www.programmableweb.com/news/63-translation-apis-bing-google-translate-and-google-ajax-language/2013/01/15)
of translation services there are two clear winners:

* Google Services
* Microsoft Services

These two are always on top of articles like "Best Machine Translation Service".

#### Google Services
The [Google Translation API](https://cloud.google.com/translate/pricing) based on usage.
Translation usage is calculated in millions of characters, where 1 million = 10^6 characters.

[Here](https://cloud.google.com/products/calculator/#id=b0c3a733-578e-4b76-be1f-d616e30170a6)
you can find a calculator.

*Based on usage* means that if you translate nothing you pay nothing, it is not
a "subscription" model.

```
Translation     $20 per 1,000,000 characters
```

Speaking in a human language: 1,000,000 characters is about **~107 991 words**
in Português. According to [this](http://www.ravi.io/language-word-lengths)
source average word is about 9.26 characters long.

How big is that? Here is an [example](http://www.megcabot.com/about-meg-cabot/frequently-asked-questions-getting-published/):

> Most adult books are about 90,000 words, and no longer than 100,000 words 


#### Microsoft Services
[The Microsoft Translator Text API](https://www.microsoft.com/en-us/translator/translatorapi.aspx)

[Here](https://azure.microsoft.com/en-us/pricing/calculator/?service=cognitive-services)
you can find a calculator, and [here](https://azure.microsoft.com/en-us/pricing/details/cognitive-services/translator-text-api/)
you can find a pricing.

I can't find where is the catch but it seems that this API is twice cheaper
and it has a free tier with a limit of 2,000,000 characters. However, free tier
isn't suitable for production use due to "no uptime guarantee" from Microsoft.
It means that our user might (or might not) experience some weird delays, which
isn't generally good.

Seems that financially Microsoft service is just better for us.


#### Conclusion
It seems that there is not much of the difference between Google Services and
Microsoft Services in terms of quality, but here is a difference in the price.

Microsoft is twice as cheap and it has a free tier which we can use for now.

---

## Infrastructure

Infrastructure means what kind of server-side solution we will use.


#### Regionality

Since we are targeting one particular area we should build our solutions around
one physical region. By *one area* I mean a region with a majority of issued requests to our bot.

This is the case because networking communication obeys physics and the most
easiest option to reduce user-to-server latency (signal time propagation) is to
move server closer to a user.

For example, physical latency from Auckland to Sao Paulo is [319.039ms](https://wondernetwork.com/pings/Auckland/Sao+Paulo).
This means that if server located in Auckland and user is located in Sao Paulo it
will take about 0.63 seconds only for signal propagation.


#### Availability

Availability for a low-load service isn't an issue in 2018.


#### Options

I can see here two possible options:

* Amazon Web Services EC2
* Amazon Web Services Lambda

Short explanation here: EC2 is a server instance, like a normal computer with
a running server program on it. Lambda is a function of demand.

**EC2 Pros**:
1. We can do lots of custom things in the future if needed. For example, we can collect statistics and do way more custom logic

**EC2 Cons**:
1. Even if bot isn't being used you still have to pay for the server
2. Infrastructure setup will take a time
3. Performance with a low tier server may suffer (not a major issue in our case)

**EC2 Conclusion** A good choice if you want to modify all the thing in the future,
especially add new features.

**[EC2 Price](https://aws.amazon.com/ec2/pricing/on-demand/)**: Highly depends on the "power". Good enough tier for us is free for
the first year and next is about $15/mo plus some minor cost for IP address, like
$0.5/mo. 

**Lambda Pros**:
1. On-demand function -> pay as you use
2. No infrastructure because no server

**Lambda Cons**:
1. Limited options for custom solutions. Basically, the whole bot is just a one (yet complex) function

**Lambda Conclusion**: Great for bot service, but suffers from the lack of infrastructure.
It is a Pro and Con at the same time. Easier to maintain (no maintenance at all),
harder to add features which needs some storage, but still possible and not that hard.

**[Lambda Price](https://aws.amazon.com/lambda/pricing/)**: Pay as you go, most likely it will be $0/mo. 
Namely $0.000000208 per 100ms of computing time. In a human language that means that
you will pay $0,208 for 1,000,000 requests,  $2,08 for 10,000,000 requests, and so on.
10,000,000 requests is **plenty**. Free tier is also available.


**Personal Suggestion**: If we do not need huge logic -- use Lambda. It has no scalability
and availability issues.


<br>
<br>
<br>

# Specification

Translation service using 3-rd party translation engine and Facebook Messenger
as a distribution platform.


## Process

Project has 3 joint dependencies:

* Messenger Integration
* Translation Engine Integration
* Controller Development (Bot's Logic)

Since these dependencies are joint project should be developed iteratively.

## Setup

At first minimum project must be built. Minimum project consists of a running
server side with a placeholder function which is integrated into Facebook Messenger
using webhooks.

After we are able to receive messages from Messenger we need to integrate translation
engine via REST API.

## Iteration

Once everything is set an iteration flow is defined as:

* Take a requirement unit
* Implement it
* Test it

Repeat this cycle until all the features are implemented.

## Delivery

Once the product is done, we should ship it and deploy into production.

## Production Deployment

We should wire our bot to desired facebook page and setup a new server environment
which belongs to client's account on AWS as well as Facebook.
